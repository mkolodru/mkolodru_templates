import numpy
import numpy.linalg
import numpy.random
import sys
from mk_qm import *

if len(sys.argv) < 2:
    print 'Usage: sort_diag_matrix.py $sz'
    sys.exit()

basis_sz=int(sys.argv[1])

# Create random Hermitian matrix
M=numpy.random.random((basis_sz,basis_sz))+1.j*numpy.random.random((basis_sz,basis_sz))
M=M+adjoint(M)

# Diagonalize it
(M_diag,U)=numpy.linalg.eigh(M)

# Check that it matches originally
diff=M-numpy.dot(U,numpy.dot(numpy.diag(M_diag),adjoint(U)))
print 'Original difference = '+str(max(numpy.abs(diff.flatten())))

# Now sort M_diag by absolute value (to change the order) and reshuffle it
inds=numpy.argsort(numpy.abs(M_diag))
N_diag=M_diag[inds]

# Finally, shuffle the columns of U to get V
V=0.*U
for j in xrange(basis_sz):
    V[:,j]=U[:,inds[j]]

# and check that is now matches
diff=M-numpy.dot(V,numpy.dot(numpy.diag(N_diag),adjoint(V)))
print 'New difference = '+str(max(numpy.abs(diff.flatten())))

print 'N_diag='+str(N_diag)
