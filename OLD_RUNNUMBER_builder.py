#! /usr/bin/env python

import subprocess
import numpy

#Log the current submission
logstr='''RUNNUMBER:



Codebase: 
Cluster: lrc
gittag: '''

#Do the git-ing
cmd='git commit -a -m "Commit before run RUNNUMBER" > /dev/null'
print cmd
subprocess.call(cmd,shell=True)

cmd='gittag.py > temp.temp'
subprocess.call(cmd,shell=True)
logstr+=open('temp.temp','r').readline()
subprocess.call('rm temp.temp',shell=True)

#subprocess.call('mkdir -p ../log_files/',shell=True)
open('RUNNUMBER.log','w').write(logstr)

#Setup the versionmap and qsub files
vmap_file=open('versionmap.dat','w')
vmap_file.write('vnum\tL\n')

task_file=open('RUNNUMBER.task','w')

template_file='RUNNUMBER.template'
template_contents=open(template_file,'r').read()

vnum=0

for L in xrange(8):
	qsub_file=template_file.replace('.template','_'+str(vnum)+'.qsub')
	fout=open(qsub_file,'w')

	contents=template_contents.replace('###',str(vnum))
	contents=contents.replace('*LLL*',str(L))
	vmap_file.write(str(vnum)+'\t'+str(L)+'\n')
	task_file.write('bash RUNNUMBER_'+str(vnum)+'.qsub\n')
	fout.write(contents)
	fout.close()
	
	vnum+=1
		
contents=open('RUNNUMBER.sbatch.template','r').read()
contents=contents.replace('*nnn*',str(vnum)) # The total number of processors
contents=contents.replace('*ttt*','02:00:00') # Time per process -- default to 2 hours
open('RUNNUMBER.sbatch','w').write(contents)

if vnum % 8 != 0:
	sys.stderr.write('Warning: Number of processors='+str(vnum)+' is not a multiple of 8\n')
