import pylab
import numpy
import mkplot
import matplotlib.font_manager
import mkfit
from read_graph_data import *
import sys
import subprocess
from mpl_toolkits.mplot3d import Axes3D

fig=pylab.figure()
ax=fig.add_subplot(111, projection='3d')

n=numpy.loadtxt('y.out')[:,:3]
x=n[:,0]
y=n[:,1]
z=n[:,2]

ax.plot([-1.5,1.5],[0,0],[0,0],color='k',linewidth=2)
ax.plot([0,0],[-1.5,1.5],[0,0],color='k',linewidth=2)
ax.plot([0,0],[0,0],[-1.5,1.5],color='k',linewidth=2)

u = numpy.linspace(0, 2 * numpy.pi, 100)
v = numpy.linspace(0, numpy.pi, 100)
xs = numpy.outer(numpy.cos(u), numpy.sin(v))
ys = numpy.outer(numpy.sin(u), numpy.sin(v))
zs = numpy.outer(numpy.ones(numpy.size(u)), numpy.cos(v))
ax.plot_surface(xs, ys, zs,  rstride=4, cstride=4, color='y', alpha=0.3, linewidth=0)

ax.plot(x,y,z,color='b',linewidth=2)

ax.set_aspect('equal')
ax.set_axis_off()
ax.view_init(40,-61)

fig_file='fig_3d.pdf'
pylab.savefig(fig_file)
pylab.savefig(fig_file.replace('pdf','png'))
if sys.argv[-1]=='show':
	pylab.show()
